# postfixadmin-import-export

Import / Export postfix domains/mailboxes for Backup / Recovery / Migration

Works only with database, use rsync or other tool to sync mailbox on disk

This tool works with records in tables: domain, mailbox, alias.

## Install

~~~
git clone https://gitlab.com/yaroslaff/postfixadmin-import-export
cd postfixadmin-import-export
pip3 install -r requirements.txt
~~~

## Options

`--export FILE` - export data to this file

`--import FILE` - import data from file

### MySQL 
`-u` - username

`-p` - password

`--db` - db name


## Export
~~~
./postfixadmin-import-export.py -u postfix -p ppass --db postfix --export backup.json example.com example.net  
~~~

## Import
~~~
./postfixadmin-import-export.py -u postfix -p ppass --db mail --import backup.json
~~~

## Sync mailboxes
Example (note: no trailing slashes!)
~~~
rsync -avz -e ssh root@source.host.net:/opt/vmail/user@example.com /var/vmail
~~~

## Tricks
Make sure both servers are using same settings, e.g. `config.inc.php`:
~~~
$CONF['encrypt'] = 'md5crypt';
~~~