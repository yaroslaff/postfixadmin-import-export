#!/usr/bin/python3

from models import *
import argparse
from peewee import MySQLDatabase
import json
import datetime

def json_converter(o):
    if isinstance(o, datetime.datetime):
        return o.__str__()

def main():
    parser = argparse.ArgumentParser(description='Import/Export postfixadmin mailboxes')

    g = parser.add_argument_group('Commands')
    g.add_argument('--show', action='store_true', default=False, help='Show user/domain')
    g.add_argument('--import', metavar='FILENAME', dest='importfile', default=None, help='Import')
    g.add_argument('--export', metavar='FILENAME', default=None, help='Export')

    g = parser.add_argument_group('Options')
    g.add_argument('domains', nargs='*', default=None, help='Export all mailboxes for domain')

    g = parser.add_argument_group('MySQL arguments')
    g.add_argument('-u', '--user', help='MySQL username')
    g.add_argument('-p', '--password', help='MySQL password')
    g.add_argument('--db', help='MySQL database name')

    args = parser.parse_args()

    db = MySQLDatabase(args.db, user=args.user, password=args.password, host='localhost', port=3306)
    db_proxy.initialize(db)

    if args.export and args.domains:
        data = list()
        for domain in args.domains:
            # print("Export domain {}".format(domain))
            d = Domain.get(Domain.domain == domain)
            # print(d.dict())
            data.append(d.export())

        with open(args.export, 'w') as f:
            json.dump(data, f, indent=4, sort_keys=True, default=json_converter)

    if args.importfile:
        with open(args.importfile, 'r') as f:
            data = json.load(f)

        for dd in data:
            d = Domain.import_data(dd)
            print("Imported", d)



if __name__ == '__main__':
    main()
